<?php

  namespace CoronaGraphTool\Highchart;

  class SetOptions {

    private $jsonFile = 'config/Highcharts.setOptions.json';

    /**
     * @param string $jsonFile path to file with json
     * @return string
     */
    private function getSetOptionsJson($jsonFile = '') {
      if (!empty($jsonFile) && is_readable($jsonFile)) {
        return file_get_contents($jsonFile);
      } elseif (is_readable($this->jsonFile)) {
        return file_get_contents($this->jsonFile);
      }
      return '';
    }

    /**
     * @param string $jsonFile path to file with json
     * @return string
     */
    public function getCode($jsonFile = '') {
      if ($setOptionsJson = $this->getSetOptionsJson($jsonFile)) {
        return sprintf(
        /** @lang javascript */
          '<script type="text/javascript">Highcharts.setOptions(%s);</script>',
          $setOptionsJson
        );
      }
      return '';
    }

  }