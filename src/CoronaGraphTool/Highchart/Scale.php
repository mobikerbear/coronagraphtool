<?php

  namespace CoronaGraphTool\Highchart;

  use CoronaGraphTool\Param\OneOfSetString;

  class Scale {
    const NAME = 'scale';
    const LINEAR = 'linear';
    const LOGARITHMIC = 'logarithmic';

    public function __construct() {
      $this->paramObject = new OneOfSetString(
        self::NAME,
        array(self::LINEAR, self::LOGARITHMIC),
        self::LINEAR
      );
    }

    public function getByParams() {
      return $this->paramObject->getParam();
    }
  }