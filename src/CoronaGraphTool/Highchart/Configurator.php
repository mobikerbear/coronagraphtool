<?php

  namespace CoronaGraphTool\Highchart;

  use CoronaGraphTool\Categories\AbstractCategory as Categories;
  use CoronaGraphTool\Categories\Factory as CategoriesFactory;
  use CoronaGraphTool\CountryData;
  use CoronaGraphTool\SeriesFetcher\AbstractSeriesFetcher as SeriesFetcher;
  use CoronaGraphTool\SeriesFetcher\Factory as SeriesFetcherFactory;

  class Configurator {
    const PathToConfigFile = 'data/allData.json';

    /** @var array */
    private $configuration;

    /** @var Categories */
    private $categories;

    /** @var SeriesFetcher */
    private $seriesFetcher;

    /** @var CountryData[] */
    private $countries;

    /** @var Scale */
    private $scale;

    public function __construct() {
      $this->scale = new Scale();
      $this->loadAndSetConfiguration();
      $this->fetchCountryDataFromConfiguration();
      $this->setCategoriesByParam();
      $this->setSeriesFetcherByParam();
    }

    private function loadAndSetConfiguration() {
      $configJson = file_get_contents(self::PathToConfigFile);
      $this->configuration = json_decode($configJson, true);
    }

    private function fetchCountryDataFromConfiguration() {
      $this->countries = array();
      foreach ($this->configuration['countries'] as $countryId => $countryDataRaw) {
        $this->countries[$countryId] = new CountryData($countryId, $countryDataRaw);
      }
      unset($this->configuration['countries']);
    }

    /**
     */
    public function setCategoriesByParam() {
      $this->categories = (new CategoriesFactory())->getByParam();
    }

    /**
     */
    public function setSeriesFetcherByParam() {
      $this->seriesFetcher = (new SeriesFetcherFactory())->getByParam();
    }

    private function getConfigurationChart() {
      return array(
        'type' => 'line',
      );
    }

    private function getConfigurationTitle() {
      $titles = array('Corona');
      if ($seriesTitle = $this->seriesFetcher->getLabel()) {
        $titles[] = $seriesTitle;
      }
      return array(
        'text' => join(' - ', $titles),
      );
    }

    private function getConfigurationXAxis() {
      return array(
        'title' => array('text' => 'Zeit'),
        'categories' => $this->getCategoriesArray(),
      );
    }

    private function getConfigurationYAxis() {
      return array(
        'title' => array('text' => 'Anzahl'),
        'type' => $this->scale->getByParams(),
      );
    }

    private function getConfigurationPlotOptions() {
      return array(
        'series' => array(
          'animation' => false,
          'marker' => array(
            'enabled' => false,
          )
        ),
      );
    }

    public function getHighChartConfigurationAsJson() {
      return json_encode(array(
        'chart' => $this->getConfigurationChart(),
        'title' => $this->getConfigurationTitle(),
        'xAxis' => $this->getConfigurationXAxis(),
        'yAxis' => $this->getConfigurationYAxis(),
        'plotOptions' => $this->getConfigurationPlotOptions(),
        'series' => $this->getAllSeriesArray()
      ), JSON_PRETTY_PRINT);
    }


    private function getCategoriesArray() {
      $array = '[]';
      foreach ($this->countries as $countryId => $countryData) {
        if (!$countryData->isActive()) continue;
        $array = $this->categories->getArray($this->seriesFetcher->getTotalAmountOfData());
        break;
      }
      return $array;
    }

    private function getAllSeriesArray() {
      $seriesItems = array();
      foreach ($this->countries as $countryId => $countryData) {
        if (!$countryData->isActive()) continue;
        $seriesItems[] = array(
          'name' => $countryData->getLabel(),
          'data' => $this->seriesFetcher->getData($countryData),
        );
      }
      return $seriesItems;
    }

    /**
     * @return string
     */
    public function getFirstDateAsUnixtime() {
      return $this->seriesFetcher->getFirstDateAsUnixtime();
    }

    /**
     * @return string
     */
    public function getLastDateAsUnixtime() {
      return $this->seriesFetcher->getLastDateAsUnixtime();
    }


  }