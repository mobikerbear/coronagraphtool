<?php

  namespace CoronaGraphTool\Categories;

  class AsDates extends AbstractCategory {
    /**
     * @param $amount
     * @return array
     */
    public function getArray($amount) {
      $categories = array();
      $currentTime = mktime(12, 0, 0, 1, 15, 2020);
      for ($i = 0; $i < $amount; $i++) {
        $categories[] = date('d. M', $currentTime);
        $currentTime += 86400;
      }
      return $categories;
    }


  }