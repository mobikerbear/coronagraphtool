<?php

  namespace CoronaGraphTool\Categories;

  abstract class AbstractCategory {
    /**
     * @param $amount
     * @return array
     */
    abstract public function getArray($amount);
  }
