<?php

  namespace CoronaGraphTool\Categories;

  class AsNumbers extends AbstractCategory {
    /**
     * @param $amount int
     * @return array
     */
    public function getArray($amount) {
      $categories = array();
      for ($i = 0; $i < $amount; $i++) {
        $categories[] = $i;
      }
      return $categories;
    }


  }