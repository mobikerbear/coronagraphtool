<?php

  namespace CoronaGraphTool\Categories;

  use CoronaGraphTool\Param\OneOfSetString;

  class Factory {
    const TYPE = 'type';
    const TYPE_DATES = 'dates';
    const TYPE_NUMBERS = 'numbers';

    public function __construct() {
      $this->paramObject = new OneOfSetString(
        self::TYPE,
        array(self::TYPE_DATES, self::TYPE_NUMBERS),
        self::TYPE_NUMBERS
      );
    }

    /**
     * @return AbstractCategory
     */
    public function getByParam() {
      switch ($this->paramObject->getParam()) {
        case self::TYPE_DATES:
          return new AsDates();
          break;
        case self::TYPE_NUMBERS:
        default:
          return new AsNumbers();
          break;
      }
    }
  }