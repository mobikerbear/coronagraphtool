<?php

  namespace CoronaGraphTool;

  class CountryData {

    /** @var integer */
    private $id;

    /** @var boolean */
    private $active;

    /** @var string */
    private $covid19id;

    /** @var string */
    private $label;

    /** @var double */
    private $population;

    /** @var string */
    private $url;

    public function __construct($id, $data) {
      $this->id = $id;
      $this->active = $data['active'];
      $this->covid19id = $data['covid19id'];
      $this->label = $data['label'];
      $this->population = $data['population'];
      $this->url = $data['url'];
    }

    /**
     * @return string
     */
    public function getCovid19id() {
      return $this->covid19id;
    }

    /**
     * @return int
     */
    public function getId() {
      return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel() {
      return $this->label;
    }

    /**
     * @return float
     */
    public function getPopulation() {
      return $this->population;
    }

    /**
     * @return string
     */
    public function getUrl() {
      return $this->url;
    }

    /**
     * @return bool
     */
    public function isActive() {
      return $this->active;
    }
  }