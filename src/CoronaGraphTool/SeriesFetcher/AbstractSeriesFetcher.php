<?php

  namespace CoronaGraphTool\SeriesFetcher;

  use CoronaGraphTool\CountryData;

  abstract class AbstractSeriesFetcher {
    /**
     * @param $countryData CountryData
     * @result array
     */
    abstract public function getData(CountryData $countryData);

    /**
     * @result string
     */
    abstract public function getLabel();

    /**
     * @result integer
     */
    abstract public function getTotalAmountOfData();

    /**
     * @return integer
     */
    abstract public function getFirstDateAsUnixtime();

    /**
     * @return integer
     */
    abstract public function getLastDateAsUnixtime();
  }