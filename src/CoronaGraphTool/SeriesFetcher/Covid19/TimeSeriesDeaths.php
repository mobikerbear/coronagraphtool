<?php

  namespace CoronaGraphTool\SeriesFetcher\Covid19;

  class TimeSeriesDeaths extends TimeSeries {

    /** @var string */
    protected $filename = 'data/COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv';

    protected $label = 'Tote';

  }