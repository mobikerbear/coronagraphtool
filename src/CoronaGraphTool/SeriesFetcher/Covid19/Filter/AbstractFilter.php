<?php


namespace CoronaGraphTool\SeriesFetcher\Covid19\Filter;


abstract class AbstractFilter {
  /**
   * @param array $data
   * @return array
   */
  abstract public function apply($data);
}