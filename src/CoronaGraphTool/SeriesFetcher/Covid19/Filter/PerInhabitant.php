<?php


namespace CoronaGraphTool\SeriesFetcher\Covid19\Filter;


use CoronaGraphTool\Param\AbstractParam;
use CoronaGraphTool\Param\OneOfSetString;

class PerInhabitant extends AbstractFilter {
  const NAME = 'perInhabitans';
  const YES = 'yes';
  const NO = 'no';

  /** @var float */
  private $inhabitants;

  /** @var AbstractParam */
  private $paramObject;

  /** @var integer */
  private $precision;

  /**
   * PerInhabitant constructor.
   * @param integer $precision
   */
  public function __construct($precision) {
    $this->precision = $precision;
    $this->getParamObject();
  }

  private function getParamObject() {
    if (empty($this->paramObject)) {
      $this->paramObject = new OneOfSetString(
        self::NAME,
        array(self::YES, self::NO),
        self::NO
      );
    }
    return $this->paramObject;
  }

  private function isActive() {
    return ($this->getParamObject())->getParam() == self::YES;
  }

  /**
   * @param float $inhabitants
   */
  public function setInhabitants($inhabitants) {
    $this->inhabitants = $inhabitants;
  }

  public function apply($data) {
    $filtered = $data;
    if ($this->isActive()) {
      foreach ($data as $index => $value) {
        $filtered[$index] = round($value / $this->inhabitants, $this->precision);
      }
    }
    return $filtered;
  }
}