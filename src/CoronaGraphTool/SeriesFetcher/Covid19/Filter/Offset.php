<?php


namespace CoronaGraphTool\SeriesFetcher\Covid19\Filter;


use CoronaGraphTool\Param\AbstractParam;
use CoronaGraphTool\Param\PositiveIntegerOrZero;

class Offset extends AbstractFilter {
  const NAME = 'offset';
  const DEFAULT = 0;
  const NOT_ACTIVE_VALUE = 0;

  /** @var integer */
  private $offset;

  /** @var AbstractParam */
  private $paramObject;

  public function __construct() {
    $this->offset = ($this->getParamObject())->getParam();
  }

  private function getParamObject() {
    if (empty($this->paramObject)) {
      $this->paramObject = new PositiveIntegerOrZero(self::NAME, self::DEFAULT);
    }
    return $this->paramObject;
  }

  private function isActive() {
    return $this->offset != self::NOT_ACTIVE_VALUE;
  }

  public function apply($data) {
    $filtered = $data;
    if ($this->isActive()) {
      $counter = 0;
      $filtered = array();
      foreach ($data as $index => $value) {
        if ($counter >= $this->offset) {
          $filtered[$index] = $value;
        }
        $counter++;
      }
    }
    return $filtered;
  }
}