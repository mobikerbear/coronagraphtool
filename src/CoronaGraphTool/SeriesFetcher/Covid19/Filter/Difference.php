<?php


  namespace CoronaGraphTool\SeriesFetcher\Covid19\Filter;


  use CoronaGraphTool\Param\AbstractParam;
  use CoronaGraphTool\Param\OneOfSetString;

  class Difference extends AbstractFilter {
    const NAME = 'difference';
    const YES = 'yes';
    const NO = 'no';

    /** @var AbstractParam */
    private $paramObject;

    public function __construct() {
      $this->getParamObject();
    }

    private function getParamObject() {
      if (empty($this->paramObject)) {
        $this->paramObject = new OneOfSetString(
          self::NAME,
          array(self::YES, self::NO),
          self::NO
        );
      }
      return $this->paramObject;
    }

    private function isActive() {
      return ($this->getParamObject())->getParam() == self::YES;
    }

    /**
     * @inheritDoc
     */
    public function apply($data) {
      $filtered = $data;
      if ($this->isActive()) {
        $previousValue = 0;
        foreach ($data as $index => $value) {
          $filtered[$index] = $value - $previousValue;
          $previousValue = $value;
        }
      }
      return $filtered;
    }
  }