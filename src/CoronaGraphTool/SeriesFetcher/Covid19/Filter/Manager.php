<?php


namespace CoronaGraphTool\SeriesFetcher\Covid19\Filter;


class Manager {
  const FILTER_INDEX_DIFFERENCE = 0;
  const FILTER_INDEX_OFFSET = 1;
  const FILTER_INDEX_THRESHOLD = 2;
  const FILTER_INDEX_PER_INHABITANT = 99;

  /** @var AbstractFilter[] */
  private $filters;

  /** @var float */
  private $inhabitants;


  public function add(AbstractFilter $filter) {
    if ($filter instanceof Offset) {
      $this->filters[self::FILTER_INDEX_OFFSET] = $filter;
    } elseif ($filter instanceof Threshold) {
      $this->filters[self::FILTER_INDEX_THRESHOLD] = $filter;
    } elseif ($filter instanceof Difference) {
      $this->filters[self::FILTER_INDEX_DIFFERENCE] = $filter;
    } elseif ($filter instanceof PerInhabitant) {
      $this->filters[self::FILTER_INDEX_PER_INHABITANT] = $filter;
    }
  }

  public function apply($data) {
    if (isset($this->filters[self::FILTER_INDEX_PER_INHABITANT])) {
      /** @var PerInhabitant $filter */
      $filter = $this->filters[self::FILTER_INDEX_PER_INHABITANT];
      $filter->setInhabitants($this->inhabitants);
    }
    foreach ($this->filters as $filter) {
      $data = $filter->apply($data);
    }
    return $data;
  }

  /**
   * @param float $inhabitants
   */
  public function setInhabitants($inhabitants) {
    $this->inhabitants = $inhabitants;
  }

}