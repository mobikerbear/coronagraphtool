<?php


  namespace CoronaGraphTool\SeriesFetcher\Covid19\Filter;

  use CoronaGraphTool\Param\AbstractParam;
  use CoronaGraphTool\Param\OneOfSetInteger;

  class Threshold extends AbstractFilter {
    const NAME = 'threshold';
    const INT_0 = 0;
    const INT_1 = 1;
    const INT_100 = 100;
    const INT_10000 = 10000;
    const NOT_ACTIVE_VALUE = 0;

    /** @var AbstractParam */
    private $paramObject;

    /** @var integer */
    private $threshold;

    /**
     * Threshold constructor.
     */
    public function __construct() {
      $this->threshold = $this->getParamObject()->getParam();
    }

    private function getParamObject() {
      if (empty($this->paramObject)) {
        $this->paramObject = new OneOfSetInteger(
          self::NAME,
          array(self::INT_0, self::INT_1, self::INT_100, self::INT_10000),
          self::INT_0
        );
      }
      return $this->paramObject;
    }

    private function isActive() {
      return $this->threshold != self::NOT_ACTIVE_VALUE;
    }

    /**
     * @inheritDoc
     */
    public function apply($data) {
      $filtered = $data;
      if ($this->isActive()) {
        $limitFailed = true;
        $filtered = array();
        foreach ($data as $index => $value) {
          if ($limitFailed && $value >= $this->threshold) {
            $limitFailed = false;
          }
          if ($limitFailed) continue;
          $filtered[$index] = $value;
        }
      }
      return $filtered;
    }
  }