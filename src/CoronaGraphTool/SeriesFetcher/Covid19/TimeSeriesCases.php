<?php

  namespace CoronaGraphTool\SeriesFetcher\Covid19;

  class TimeSeriesCases extends TimeSeries {

    /** @var string */
    protected $filename = 'data/COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv';

    /** @var string */
    protected $label = 'Fälle';
  }