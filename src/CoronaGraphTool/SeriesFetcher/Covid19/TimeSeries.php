<?php

  namespace CoronaGraphTool\SeriesFetcher\Covid19;

  use CoronaGraphTool\CountryData;
  use CoronaGraphTool\SeriesFetcher\AbstractSeriesFetcher;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\AbstractFilter;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\Manager;

  abstract class TimeSeries extends AbstractSeriesFetcher {
    const PATTERN_KEY_DATA = '~^(\d{1,2})/(\d{1,2})/(\d{1,2})$~';
    const COUNTRY = 'Country/Region';
    const PROVINCE = 'Province/State';

    /** @var string */
    protected $filename = '';

    /** @var Manager */
    private $filterManager;

    /** @var array */
    static private $allData;

    /** @var array */
    static private $fileData;

    /** @var int */
    static private $firstDate;

    /** @var string[] */
    static private $keys;

    /** @var int */
    static private $keyProvince = 0;

    /** @var int */
    static private $keyCountry = 0;

    /** @var string */
    protected $label = '';

    /** @var int */
    static private $lastDate;

    /** @var integer */
    static private $totalAmount = 0;

    public function __construct() {
      $this->prepareFilterManager();
      $this->loadFileData();
      $this->readFileData();
      self::$fileData = '';
    }

    private function prepareFilterManager() {
      $this->filterManager = new Manager();
    }

    /**
     * @return string
     */
    public function getLabel() {
      return $this->label;
    }

    public function addFilter(AbstractFilter $filter) {
      $this->filterManager->add($filter);
      return $this;
    }

    /**
     * @inheritDoc
     */
    public function getData(CountryData $countryData) {
      $this->filterManager->setInhabitants($countryData->getPopulation());
      if (isset(self::$allData[$countryData->getCovid19id()][''])) {
        $data = self::$allData[$countryData->getCovid19id()][''];
      }
      return array_values($this->applyFilters($data));
    }

    private function applyFilters($data) {
      return $this->filterManager->apply($data);
    }

    private function loadFileData() {
      if (empty(self::$fileData)) {
        self::$fileData = file($this->filename);
      }
    }

    private function readFileData() {
      self::$allData = array();
      foreach (self::$fileData as $line) {
        $row = $this->readCSVLine($line);
        if (empty(self::$keys)) {
          self::$keys = $row;
          self::$keyCountry = array_search(self::COUNTRY, self::$keys);
          self::$keyProvince = array_search(self::PROVINCE, self::$keys);
        } else {
          $this->addRowToData($row);
        }
      }
    }

    /**
     * @param $row array
     */
    private function addRowToData($row) {
      $country = $row[self::$keyCountry];
      $province = $row[self::$keyProvince];
      self::$allData[$country][$province] = $this->extractDataFromRow($row);
    }

    private function readCSVLine($line) {
      return explode(',', str_replace(array("\n", "\r"), '', $line));
    }

    /**
     * @return int
     */
    public function getFirstDateAsUnixtime() {
      if (empty(self::$firstDate)) {
        foreach (self::$keys as $index => $key) {
          if (preg_match(self::PATTERN_KEY_DATA, $key, $m)) {
            self::$firstDate = mktime(12, 0, 0, $m[1], $m[2], $m[3]);
            break;
          }
        }
      }
      return (int)self::$firstDate;
    }

    /**
     * @return int
     */
    public function getLastDateAsUnixtime() {
      if (empty(self::$lastDate)) {
        if (preg_match(self::PATTERN_KEY_DATA, end(self::$keys), $m)) {
          self::$lastDate = mktime(12, 0, 0, $m[1], $m[2], $m[3]);
        }
      }
      return (int)self::$lastDate;
    }

    public function getTotalAmountOfData() {
      if (empty(self::$totalAmount)) {
        self::$totalAmount = 0;
        foreach (self::$keys as $index => $key) {
          if (preg_match(self::PATTERN_KEY_DATA, $key)) {
            self::$totalAmount++;
          }
        }
      }
      return self::$totalAmount;
    }

    /**
     * @param array $row
     * @return array
     */
    private function extractDataFromRow(array $row) {
      $data = array();
      foreach (self::$keys as $index => $key) {
        if (preg_match(self::PATTERN_KEY_DATA, $key)) {
          $data[$key] = (int)$row[$index];
        }
      }
      return $data;
    }
  }