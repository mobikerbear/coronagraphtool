<?php

  namespace CoronaGraphTool\SeriesFetcher;

  use CoronaGraphTool\Param\OneOfSetString;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\Difference;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\Offset;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\PerInhabitant;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\Threshold;
  use CoronaGraphTool\SeriesFetcher\Covid19\TimeSeriesCases;
  use CoronaGraphTool\SeriesFetcher\Covid19\TimeSeriesDeaths;

  class Factory {
    const PER_INHABITANTS_PRECESION = 2;
    const WHAT = 'what';
    const WHAT_DEATHS = 'deaths';
    const WHAT_CASES = 'cases';

    private $what;

    public function __construct() {
      $this->what = new OneOfSetString(
        self::WHAT,
        array(self::WHAT_CASES, self::WHAT_DEATHS),
        self::WHAT_CASES
      );
    }

    public function getByParam() {

      switch ($this->what->getParam()) {
        case self::WHAT_CASES:
        default:
          $product = new TimeSeriesCases();
          break;
        case self::WHAT_DEATHS:
          $product = new TimeSeriesDeaths();
          break;
      }
      $product
        ->addFilter(new Difference())
        ->addFilter(new Offset())
        ->addFilter(new Threshold())
        ->addFilter(new PerInhabitant(self::PER_INHABITANTS_PRECESION));
      return $product;
    }
  }
