<?php

  namespace CoronaGraphTool\Param;

  class OneOfSetString extends AbstractParam {

    /** @var string[] */
    private $set;

    /**
     * ObjectOneOfSet constructor.
     * @param $name string
     * @param $set string[]
     * @param $default string
     */
    public function __construct($name, $set, $default) {
      $this->name = $name;
      $this->set = $set;
      $this->default = $default;
      $this->registerMe();
    }

    /**
     * @param $param string
     * @return bool
     */
    private function isParamValid($param) {
      return (in_array($param, $this->set));
    }

    /**
     * @return mixed|string
     */
    public function getParam() {
      $param = filter_input(INPUT_GET, $this->name);
      if (!$this->isParamValid($param)) {
        $param = $this->default;
      }
      return $param;
    }
  }