<?php

  namespace CoronaGraphTool\Param;

  class OneOfSetInteger extends AbstractParam {

    /** @var integer[] */
    private $set;

    /**
     * ObjectOneOfSet constructor.
     * @param $name string
     * @param $set integer[]
     * @param $default integer
     */
    public function __construct($name, $set, $default) {
      $this->name = $name;
      $this->set = $set;
      $this->default = $default;
      $this->registerMe();
    }

    /**
     * @param $param string
     * @return bool
     */
    private function isParamValid($param) {
      return (in_array($param, $this->set));
    }

    /**
     * @return integer
     */
    public function getParam() {
      $param = filter_input(INPUT_GET, $this->name, FILTER_VALIDATE_INT);
      if (!$this->isParamValid($param)) {
        $param = $this->default;
      }
      return (int)$param;
    }
  }