<?php


namespace CoronaGraphTool\Param;


class PositiveIntegerOrZero extends AbstractParam {

  /**
   * ObjectOneOfSet constructor.
   * @param $name string
   * @param $set integer[]
   * @param $default integer
   */
  public function __construct($name, $default) {
    $this->name = $name;
    $this->default = $default;
    $this->registerMe();
  }

  /**
   * @param $param string
   * @return bool
   */
  private function isParamValid($param) {
    return ((int)$param >= 0);
  }

  /**
   * @return integer
   */
  public function getParam() {
    $param = filter_input(INPUT_GET, $this->name, FILTER_VALIDATE_INT);
    if (!$this->isParamValid($param)) {
      $param = $this->default;
    }
    return (int)$param;
  }}