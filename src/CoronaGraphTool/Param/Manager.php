<?php

  namespace CoronaGraphTool\Param;

  class Manager {

    static private $instance;

    /** @var AbstractParam[] */
    private $paramObjects;

    private function __construct() {
    }

    static public function getInstance() {
      if (!self::$instance instanceof Manager) {
        self::$instance = new Manager();
      }
      return self::$instance;
    }

    public function registerParameter(AbstractParam $paramObject) {
      $name = $paramObject->getName();
      if (!isset($this->paramObjects[$name])) {
        $this->paramObjects[$name] = $paramObject;
        ksort($this->paramObjects);
      }
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getByName($name) {
      if (isset($this->paramObjects[$name])) {
        return $this->paramObjects[$name]->getParam();
      }
      return null;
    }

    /**
     * @return array
     */
    public function getCurrentAsArray() {
      $current = array();
      foreach ($this->paramObjects as $name => $paramObject) {
        $current[$name] = $paramObject->getParam();
      }
      return $current;
    }

    /**
     * @param array $overwriteParams
     * @param AbstractParam $paramObject
     * @return string
     */
    private function getOverwriteOrGivenValue($overwriteParams, AbstractParam $paramObject) {
      $name = $paramObject->getName();
      return (isset($overwriteParams[$name]))
        ? $overwriteParams[$name]
        : $paramObject->getParam();
    }

    /**
     * @param $name string
     * @param $value string
     * @return string
     */
    private function getUrlNameValuePair($name, $value) {
      return sprintf('%s=%s',
        urlencode($name),
        urlencode($value)
      );
    }

    /**
     * @param array $overwriteParams
     * @return string
     */
    public function getCurrentAsUrlParams($overwriteParams = array()) {
      $urlParams = array();
      foreach ($this->paramObjects as $name => $paramObject) {
        $value = $this->getOverwriteOrGivenValue($overwriteParams, $paramObject);
        $urlParams[] = $this->getUrlNameValuePair($name, $value);
      }
      return implode('&amp;', $urlParams);
    }

    public function dump() {
      var_dump($this->paramObjects);
    }
  }