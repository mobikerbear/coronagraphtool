<?php

  namespace CoronaGraphTool\Param;

  abstract class AbstractParam {
    /** @var string */
    protected $name;

    /** @var mixed */
    protected $default;

    /**
     * @return string
     */
    public function getName() {
      return $this->name;
    }

    public function registerMe() {
      $paramManager = Manager::getInstance();
      $paramManager->registerParameter($this);
    }

    abstract public function getParam();

  }