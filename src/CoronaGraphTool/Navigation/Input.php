<?php


  namespace CoronaGraphTool\Navigation;


  use CoronaGraphTool\Param\Manager;

  class Input extends AbstractNavigation {
    /** @var string */
    private $name;

    /** @var array */
    private $overwriteParams;

    /** @var Manager */
    private $paramManager;

    /**
     * LinkButton constructor.
     * @param string $name
     */
    public function __construct($name, $overwriteParams = array()) {
      $this->paramManager = Manager::getInstance();
      $this->name = $name;
      $this->overwriteParams = $overwriteParams;
    }

    private function wrapForm($inner) {
      return sprintf('<form method="get">%s</form>', $inner);
    }

    private function getHiddenFields() {
      $hidden = '';
      foreach ($this->paramManager->getCurrentAsArray() as $name => $value) {
        if ($name == $this->name) continue;
        if (isset($this->overwriteParams[$name])) {
          $value = $this->overwriteParams[$name];
        }
        $hidden .= sprintf(
          '<input type="hidden" name="%s" value="%s">',
          $name, $value
        );
      }
      return $hidden;
    }

    private function getInputField() {
      return sprintf(
        '<input type="text" name="%s" value="%s">',
        $this->name, $this->paramManager->getByName($this->name)
      );
    }

    public function get() {
      return $this->wrapForm($this->getHiddenFields() . $this->getInputField());
    }

    public function getClassAttribute() {
      return ' class="form"';
    }


  }