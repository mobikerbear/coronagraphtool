<?php

  namespace CoronaGraphTool\Navigation;

  abstract class AbstractNavigation {
    private $parent;

    abstract public function get();

    /**
     * @return string
     */
    public function getClassAttribute() {
      return '';
    }

    /**
     * @param AbstractNavigation $parent
     * @return $this
     */
    public function setParent(AbstractNavigation $parent) {
      $this->parent = $parent;
      return $this;
    }

  }