<?php

  namespace CoronaGraphTool\Navigation;

  class Group extends AbstractNavigation {
    /** @var string */
    private $class = '';

    /** @var AbstractNavigation[] */
    private $list;

    public function __construct($class = '') {
      $this->class = $class;
    }

    public function add(AbstractNavigation $link) {
      $this->list[] = $link->setParent($this);
      return $this;
    }

    public function get() {
      $inner = '';
      foreach ($this->list as $link) {
        $inner .= sprintf('<li%s>%s</li>',
          $link->getClassAttribute(),
          $link->get()
        );
      }
      return sprintf('<ul%s>%s</ul>',
        $this->getInnerClassAttribute(),
        $inner
      );
    }

    private function getInnerClassAttribute() {
      if (!empty($this->class)) {
        return sprintf(' class="%s"', $this->class);
      }
      return '';
    }

  }