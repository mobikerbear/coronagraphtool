<?php

  namespace CoronaGraphTool\Navigation;

  use CoronaGraphTool\Param\Manager;

  class Link extends AbstractNavigation {
    const SEP_QMARK = '?';
    const SEP_AMP = '&amp;';

    /** @var string */
    private $baseUrl;

    /** @var string */
    private $label;

    /** @var Manager */
    private $paramManager;

    /** @var array */
    private $params;

    /**
     * LinkButton constructor.
     * @param string $label
     * @param string $baseUrl
     * @param array $params
     */
    public function __construct($label, $baseUrl, $params) {
      $this->paramManager = Manager::getInstance();
      $this->label = $label;
      $this->baseUrl = $baseUrl;
      $this->params = $params;
    }

    /**
     * @return string
     */
    public function getHref() {
      $separator = (preg_match('~^.+\?.+$~', $this->baseUrl))
        ? self::SEP_AMP : self::SEP_QMARK;
      return $this->baseUrl . $separator . $this->paramManager->getCurrentAsUrlParams($this->params);
    }

    /**
     * @return string
     */
    public function get() {
      return sprintf('<a href="%s">%s</a>',
        $this->getHref(),
        $this->label
      );
    }

    /**
     * @return string
     */
    public function getClassAttribute() {
      $match = true;
      foreach ($this->params as $name => $value) {
        if ($this->paramManager->getByName($name) !== $value) {
          $match = false;
          break;
        }
      }
      if ($match) {
        return ' class="active"';
      }
      return '';
    }

  }