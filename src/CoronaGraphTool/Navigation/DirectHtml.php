<?php


  namespace CoronaGraphTool\Navigation;


  class DirectHtml extends AbstractNavigation {
    /** @var string */
    private $html;

    /**
     * LinkButton constructor.
     * @param string $html
     */
    public function __construct($html) {
      $this->html = $html;
    }

    public function get() {
      return $this->html;
    }

    public function getClassAttribute() {
      return '';
    }

  }