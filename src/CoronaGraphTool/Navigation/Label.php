<?php


  namespace CoronaGraphTool\Navigation;


  class Label extends AbstractNavigation {

    /** @var string */
    private $label;

    /**
     * LinkButton constructor.
     * @param string $label
     */
    public function __construct($label) {
      $this->label = $label;
    }

    public function get() {
      return $this->label;
    }

    public function getClassAttribute() {
      return ' class="label"';
    }

  }