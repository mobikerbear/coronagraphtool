<!DOCTYPE html>
<html lang="de">
<?php

  use CoronaGraphTool\Highchart\Configurator as HighchartConfigurator;
  use CoronaGraphTool\Highchart\Scale;
  use CoronaGraphTool\Highchart\SetOptions;
  use CoronaGraphTool\Navigation\Input;
  use CoronaGraphTool\Navigation\Label;
  use CoronaGraphTool\Navigation\Link;
  use CoronaGraphTool\Navigation\Group;
  use CoronaGraphTool\Param\Manager as ParamManager;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\Difference;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\Offset;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\PerInhabitant;
  use CoronaGraphTool\SeriesFetcher\Covid19\Filter\Threshold;
  use CoronaGraphTool\SeriesFetcher\Factory as SeriesFetcherFactory;

  include_once 'vendor/autoload.php';

  $hcConfigurator = new HighchartConfigurator();
?>
<head>
  <meta charset="UTF-8">
  <title>Corona Graph Tool</title>
  <script type="text/javascript" src="js/highcharts.js"></script>
  <link rel="stylesheet" href="css/coronagraphtool.css"/>
</head>
<body>
<p>
  <a href="manual.html" target="manual">Anleitung</a> |
  <a href="links.html" target="manual">Links</a> |
  <a href="index.php?<?php echo ParamManager::getInstance()->getCurrentAsUrlParams(); ?>">Link zum Teilen</a> |
  <a href="index.php">Reset</a>
</p>
<nav>
  <?php
    $baseUrl = 'index.php';
    echo (new Group('groups'))
      ->add((new Group('what'))
        ->add(new Label('Was'))
        ->add(new Link('Fälle', $baseUrl, array(SeriesFetcherFactory::WHAT => SeriesFetcherFactory::WHAT_CASES)))
        ->add(new Link('Tote', $baseUrl, array(SeriesFetcherFactory::WHAT => SeriesFetcherFactory::WHAT_DEATHS)))
      )
      ->add((new Group('scale'))
        ->add(new Label('Skala'))
        ->add(new Link('linear', $baseUrl, array(Scale::NAME => Scale::LINEAR)))
        ->add(new Link('logarithmic', $baseUrl, array(Scale::NAME => Scale::LOGARITHMIC)))
      )
      ->add((new Group('difference'))
        ->add(new Label('Gesamt/tägl.'))
        ->add(new Link('Gesamt', $baseUrl, array(Difference::NAME => Difference::NO)))
        ->add(new Link('täglich', $baseUrl, array(Difference::NAME => Difference::YES)))
      )
      ->add((new Group('threshold'))
        ->add(new Label('Schwellwert'))
        ->add(new Label('&nbsp;'))
        ->add(new Link('0', $baseUrl, array(Threshold::NAME => Threshold::INT_0)))
        ->add(new Link('1', $baseUrl, array(Threshold::NAME => Threshold::INT_1, Offset::NAME => Offset::NOT_ACTIVE_VALUE)))
        ->add(new Link('100', $baseUrl, array(Threshold::NAME => Threshold::INT_100, Offset::NAME => Offset::NOT_ACTIVE_VALUE)))
        ->add(new Link('10000', $baseUrl, array(Threshold::NAME => Threshold::INT_10000, Offset::NAME => Offset::NOT_ACTIVE_VALUE)))
      )
      ->add((new Group('offset'))
        ->add(new Label('Start'))
        ->add(new Input('offset', array(Threshold::NAME => Threshold::NOT_ACTIVE_VALUE)))
      )
      ->add((new Group('per_inhabitants'))
        ->add(new Label('Bezug?'))
        ->add(new Link('absolut', $baseUrl, array(PerInhabitant::NAME => PerInhabitant::NO)))
        ->add(new Link('pro Einwohner', $baseUrl, array(PerInhabitant::NAME => PerInhabitant::YES)))
      )
      ->get();
  ?>
</nav>
<?php echo (new SetOptions())->getCode(); ?>
<div id="container" style="width:100%; height:600px;"></div>
<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function () {
    Highcharts.chart('container', <?php echo $hcConfigurator->getHighChartConfigurationAsJson(); ?>);
  });</script>
<p>Datenquelle: <a
    href="https://github.com/CSSEGISandData/COVID-19.git"
    target="_blank">https://github.com/CSSEGISandData/COVID-19.git</a> (Ich
  aktualisiere die Daten nicht täglich.)</p>
<p>Daten vom <?php echo date('d.m.Y', $hcConfigurator->getFirstDateAsUnixtime()); ?> bis
  zum <?php echo date('d.m.Y', $hcConfigurator->getLastDateAsUnixtime()); ?>. Bei
  Filtern kann die angezeigte Datenmenge kleiner sein. </p>
<script type="text/javascript">window.name = "graph";</script>
</body>
</html>