#!/bin/bash

cat << DELIMITER >> /etc/hosts

127.0.0.1    coronagraphtool.lan
DELIMITER

chmod 777 /tmp/xdebug/profiles
chmod 777 /tmp/xdebug/traces

exec /usr/sbin/apachectl -DFOREGROUND
