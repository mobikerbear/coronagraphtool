# CoronaGraphTool

The CoronaGraphTool shall help to visualize the given data from https://github.com/CSSEGISandData/COVID-19.git in a graph.

It uses Highcharts (https://www.highcharts.com/). Consider the license for Highcharts if you want to use my tool for your own. 

## Install

Copy all files in folder `src` to the webserver except the following folders:

- `data/COVID-19`
- `vendor`

### Data

For the data there are currently just two files used in folder `data/COVID-19/csse_covid_19_data/csse_covid_19_time_series/`:

- `time_series_covid19_confirmed_global.csv`
- `time_series_covid19_deaths_global.csv`

All other data is not needed for now. 

### Composer enviroment

Establish the composer environment. If this is not possible on the server because of access restrictions, you have to build the environment locally and copy all files in `vendor`. 

## Docker

There is a docker environment. Copy `docker-compose-dist.yml` to `docker-compose.yml` and change the port and probably other settings due to your needs. 

Don't forget to add an entry for `coronagraphtool.lan` in `/etc/hosts`.